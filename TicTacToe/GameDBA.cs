﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.DBLayer;
using TicTacToe.DomainLayer;
using System.Data;
using System.Collections.ObjectModel;
using System.Windows;
using PostSharp.Patterns.Model;


namespace TicTacToe
{
    [NotifyPropertyChanged]

    class GameDBA
    {
        DBConn db;
        public GameDBA()
        {
            db = new DBConn("/DBLayer", "tictactoe.s3db");
            String createSQL = "CREATE TABLE IF NOT EXISTS GAME (id INTEGER PRIMARY KEY NOT NULL, player1 TEXT NOT NULL, player2 TEXT NOT NULL, turn INT NOT NULL DEFAULT 0, state TEXT NOT NULL)";
            db.ExecuteNonQuery(createSQL);
        }

        public Game LoadGame(int gameID)
        {
            String theQuery = "select * from game where id = " + gameID + ";";
            DataTable dt = db.GetDataTable(theQuery);
            Game theGame = new Game();
            String[] states = new String[9];
            foreach (DataRow row in dt.Rows)
            {
                theGame.GameID = gameID;
                theGame.Player1 = GetPlayer(row["player1"].ToString());
                theGame.Player2 = GetPlayer(row["player2"].ToString());
                theGame.Turn = int.Parse(row["turn"].ToString());
                theGame.States = row["state"].ToString().Split(',');
            }
            if (theGame.GameID == gameID)
                return theGame;
            else
                return null;
        }

        public DataTable GetGamesDataTable()
        {
            String theQuery = "select id,player1,player2,turn from game order by id;";
            DataTable dt = db.GetDataTable(theQuery);

            return dt;
        }

        public bool SaveGame(Game theGame)
        {
            String theStates = "";
            Dictionary<String, String> gameState = new Dictionary<String, String>();
            gameState.Add("player1", theGame.Player1.Name);
            gameState.Add("player2", theGame.Player2.Name);
            gameState.Add("turn", theGame.Turn.ToString());
            foreach (string s in theGame.States)
            {
                theStates += s + ",";
            }
            gameState.Add("state", theStates);

            if (theGame.GameID == 0)
            {
                String theQuery = "select max(id) from game;";
                String theID = db.ExecuteScalar(theQuery);
                if (theID == "")
                    theID = "0";
                int theIDNum = Int32.Parse(theID);
                theIDNum++;
                gameState.Add("id", theIDNum.ToString());
                theGame.GameID = theIDNum;
                return db.Insert("game", gameState);
            }
            else
            {
                gameState.Add("id", theGame.GameID.ToString());
                //MessageBox.Show("update: " + theStates);
                return db.Update("game", gameState, "id=" + theGame.GameID);
            }
        }

        public Player GetPlayer(string pname)
        {
            String theQuery = "select * from player where name = '" + pname + "';";
            DataTable dt = db.GetDataTable(theQuery);
            Player thePlayer = new Player();

            foreach (DataRow row in dt.Rows)
            {
                thePlayer.Name = pname;
                thePlayer.Wins = int.Parse(row["wins"].ToString());
                thePlayer.Losses = int.Parse(row["losses"].ToString());
                thePlayer.Draws = int.Parse(row["draws"].ToString());
                thePlayer.Streak = int.Parse(row["streak"].ToString());
                thePlayer.Ratio = decimal.Parse(row["ratio"].ToString());
                thePlayer.DidWinlast = row["didWin"].ToString()[0];
            }

            return thePlayer;
        }
    }
}
