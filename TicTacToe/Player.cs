﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using TicTacToe;
using PostSharp.Patterns.Model;

namespace TicTacToe
{
    [NotifyPropertyChanged]
    public class Player
    {
        // Field 
        private string name;
        private int wins, losses, draws, streak;
        private decimal ratio;
        private char didWinLast;

        // Properties
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
            }
        }

        public int Wins
        {
            get { return wins; }
            set
            {
                wins = value;
            }
        }

        public int Losses
        {
            get { return losses; }
            set
            {
                losses = value;
            }
        }

        public int Draws
        {
            get { return draws; }
            set
            {
                draws = value;
            }
        }

        public decimal Ratio
        {
            get { return ratio; }
            set
            {
                ratio = value;
            }
        }

        public int Streak
        {
            get { return streak; }
            set
            {
                streak = value;
            }
        }

        public char DidWinlast
        {
            get { return didWinLast; }
            set
            {
                didWinLast = value;
            }
        }

        // Constructor that takes no arguments. 
        public Player()
        {
            name = "unknown";
            wins = losses = draws = streak = 0;
            ratio = (decimal)0.00;
            didWinLast = 'N';
        }

        // Constructor that takes arguments. 
        public Player(string name, int wins, int losses, int draws, int streak, char didWinLast)
        {
            this.name = name;
            this.wins = wins;
            this.losses = losses;
            this.draws = draws;
            this.streak = streak;
            this.didWinLast = didWinLast;
            if ((wins + losses) > 0)
                this.ratio = (decimal)(wins) / (decimal)((decimal)wins + (decimal)losses) * 100m;
            else
                this.ratio = 0;
        }

        // Method 
        public void AddToStats(char status)
        {
            if (status == 'W')
            {
                Wins = Wins + 1;
            }
            else if (status == 'L')
            {
                Losses = Losses + 1;
            }
            else if (status == 'D')
            {
                Draws = Draws + 1;
            }
        }
    }
}
