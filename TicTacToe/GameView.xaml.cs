﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TicTacToe.DomainLayer;
using TicTacToe.ViewModels;

namespace TicTacToe
{
    /// <summary>
    /// Interaction logic for GameUserControl.xaml
    /// </summary>
    public partial class GameView : UserControl
    {
       

        public GameView(String p1, String p2, Game theGame)
        {
            InitializeComponent();
            this.DataContext = new GameViewModel(p1,p2, theGame);

        }

        private void DoneButton_Click(object sender, RoutedEventArgs e)
        {
            (Application.Current.MainWindow.FindName("contentControl") as ContentControl).Content = new WelcomeView();
        }

      
    }
}
