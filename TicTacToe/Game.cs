﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PostSharp.Patterns.Model;
using System.Windows;
using System.IO;

namespace TicTacToe.DomainLayer
{
    [NotifyPropertyChanged]
    public class Game
    {
        Player _player1;
        Player _player2;
        int _turn;
        int _gameID = 0; 
        String[] _states = new String[9];

        public Game()
        {
            _turn = 0;
            for (int i = 0; i < _states.Length; i++)
            {
                _states[i] = "N";
            }
        }

        public int GameID 
        {
            get { return _gameID; }
            set
            {
                _gameID = value;
            }
        }

        public Player Player1
        {
            get { return _player1; }
            set
            {
                _player1 = value;
            }
        }

        public Player Player2
        {
            get { return _player2; }
            set
            {
                _player2 = value;
            }
        }

        public String[] States
        {
            get { return _states; }
            set
            {
                _states = value;
            }
        }

        public int Turn
        {
            get { return _turn; }
            set
            {
                _turn = value;
            }
        }

        public int getTurn()
        {
            return _turn;
        }

        public void incTurn()
        {
            _turn++;
        }

        public void resetTurn()
        {
            _turn = 0;
        }


    }
}
