﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.DBLayer;
using System.Data;
using System.Collections.ObjectModel;
using System.Windows;
using PostSharp.Patterns.Model;


namespace TicTacToe
{
    [NotifyPropertyChanged]

    class PlayerDBA
    {
        string name, wins, ratio, theQuery, streak;
        DBConn db;
        public PlayerDBA()
        {
            db = new DBConn("/DBLayer", "tictactoe.s3db");
            string createSQL = "CREATE TABLE IF NOT EXISTS PLAYER (name TEXT PRIMARY KEY NOT NULL, wins INT NOT NULL DEFAULT 0, losses INT NOT NULL DEFAULT 0, draws INT NOT NULL DEFAULT 0, ratio NUMERIC(5,2) NOT NULL DEFAULT 0.00, streak INT NOT NULL DEFAULT 0, didwin CHAR(1) NOT NULL DEFAULT 'N')";
            db.ExecuteNonQuery(createSQL);
        }

        private string calcRatio(int wins, int losses)
        {
            decimal ratio;
            float winsf = (float)wins;
            float lossesf = (float)losses;
            if (wins + losses == 1)
            {
                if (wins == 1)
                    ratio = 1;
                else
                    ratio = 0;
                return string.Format("{0}%", ratio); ;
            }
            else
            {

                ratio = (decimal)(winsf / (winsf + lossesf));
                return string.Format("{0:N2}%", ratio);
            }
        }

        public void PopulateInitialPlayers()
        {


            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("name", "Chris");
            data.Add("wins", "2");
            data.Add("losses", "1");
            data.Add("streak", "1");
            data.Add("ratio", calcRatio(2, 1));
            db.Insert("player", data);
            data.Clear();

            data.Add("name", "Yann");
            data.Add("wins", "4");
            data.Add("losses", "2");
            data.Add("streak", "3");
            data.Add("ratio", calcRatio(4, 2));
            db.Insert("player", data);
            data.Clear();

            data.Add("name", "Peter");
            data.Add("wins", "1");
            data.Add("losses", "2");
            data.Add("draws", "1");
            data.Add("ratio", calcRatio(1, 2));
            db.Insert("player", data);
            data.Clear();

            data.Add("name", "Josh");
            data.Add("wins", "4");
            data.Add("losses", "1");
            data.Add("streak", "2");
            data.Add("ratio", calcRatio(4, 1));
            db.Insert("player", data);
            data.Clear();

            data.Add("name", "Cameron");
            data.Add("wins", "1");
            data.Add("losses", "1");
            data.Add("ratio", calcRatio(1, 1));
            db.Insert("player", data);
            data.Clear();
        }

        public Dictionary<String, String> GetMostWins()
        {
            Dictionary<String, String> stats = new Dictionary<String, String>();
            theQuery = "select * from player where wins = (select max(wins) from player) ORDER BY wins DESC, name ASC LIMIT 1;";
            DataTable dt = db.GetDataTable(theQuery);

            foreach (DataRow row in dt.Rows)
            {
                name = row["name"].ToString();
                wins = row["wins"].ToString();
            }

            stats.Add("name", name);
            stats.Add("wins", wins);

            return stats;
        }

        public Dictionary<String, String> GetLongestStreak()
        {
            Dictionary<String, String> stats = new Dictionary<String, String>();
            theQuery = "select * from player where streak = (select max(streak) from player) ORDER BY streak, wins DESC, name ASC LIMIT 1;";
            DataTable dt = db.GetDataTable(theQuery);

            foreach (DataRow row in dt.Rows)
            {
                name = row["name"].ToString();
                streak = row["streak"].ToString();
            }
            stats.Add("name", name);
            stats.Add("streak", streak);

            return stats;
        }

        public bool CheckPlayer(String name)
        {
            if (name != "")
            {
                theQuery = "select name from player where name = '" + name + "';";
                String result = db.ExecuteScalar(theQuery);
                if (result == name)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        public Dictionary<String, String> GetBestRatio()
        {
            Dictionary<String, String> stats = new Dictionary<String, String>();
            theQuery = "select * from player where ratio = (select max(ratio) from player) ORDER BY ratio DESC, wins DESC, name ASC LIMIT 1;";
            DataTable dt = db.GetDataTable(theQuery);

            foreach (DataRow row in dt.Rows)
            {
                name = row["name"].ToString();
                ratio = row["ratio"].ToString() + "%";
            }


            stats.Add("name", name);
            stats.Add("ratio", ratio);

            return stats;
        }

        public ObservableCollection<Player> GetPlayersDataTable()
        {
            String theQuery = "select * from player order by name asc;";
            DataTable dt = db.GetDataTable(theQuery);
            ObservableCollection<Player> PlayersOCforDT = new ObservableCollection<Player>();
            foreach(DataRow row in dt.Rows)
            {
                var obj = new Player()
                {
                    Name = row["name"].ToString(),
                    Wins = (int)row["wins"],
                    Losses = (int)row["losses"],
                    Draws = (int)row["draws"],
                    Streak = (int)row["streak"],
                    Ratio = (decimal)row["ratio"],
                    DidWinlast = row["didWin"].ToString()[0]
                };
                PlayersOCforDT.Add(obj);
             }
            return PlayersOCforDT;
        }

        public List<Player> GetMatchPlayers(string p1name, string p2name)
        {
            String theQuery = "select * from player where name = " + p1name + " and name = " + p2name + ";";
            DataTable dt = db.GetDataTable(theQuery);
            List<Player> list = new List<Player>();
            String name;
            int wins, losses, draws, streak;
            decimal ratio;
            char didWin;
            foreach (DataRow row in dt.Rows)
            {
                name = row["name"].ToString();
                wins = int.Parse(row["wins"].ToString());
                losses = int.Parse(row["losses"].ToString());
                draws = int.Parse(row["draws"].ToString());
                streak = int.Parse(row["streak"].ToString());
                ratio = decimal.Parse(row["ratio"].ToString());
                didWin = row["didWin"].ToString()[0];
                list.Add(new Player(name, wins, losses, draws, streak, didWin));
            }
            return list;
        }

        public Player GetPlayer(string pname)
        {
            String theQuery = "select * from player where name = '" + pname + "';";
            DataTable dt = db.GetDataTable(theQuery);
            Player thePlayer = new Player();

            foreach (DataRow row in dt.Rows)
            {
                thePlayer.Name = pname;
                thePlayer.Wins = int.Parse(row["wins"].ToString());
                thePlayer.Losses = int.Parse(row["losses"].ToString());
                thePlayer.Draws = int.Parse(row["draws"].ToString());
                thePlayer.Streak = int.Parse(row["streak"].ToString());
                thePlayer.Ratio = decimal.Parse(row["ratio"].ToString());
                thePlayer.DidWinlast = row["didWin"].ToString()[0];
            }

            return thePlayer;
        }

        public bool CreateNewPlayer(Player newPlayer)
        {
            Dictionary<String, String> stats = new Dictionary<String, String>();
            stats.Add("name", newPlayer.Name);
            stats.Add("wins", "0");
            stats.Add("losses", "0");
            stats.Add("draws", "0");
            stats.Add("streak", "0");
            stats.Add("ratio", "0");
            stats.Add("didWin", "N");
            return db.Insert("player", stats);
        }

        public bool UpdatePlayer(Player thePlayer, String status)
        {
            Dictionary<String, String> stats = new Dictionary<String, String>();
            if (status == "win")
            {
                stats.Add("wins", thePlayer.Wins + 1 + "");
                stats.Add("streak", thePlayer.Streak + 1 + "");
                stats.Add("didWin", "Y");
                stats.Add("ratio", calcRatio(thePlayer.Wins + 1, thePlayer.Losses));
            }
            else if (status == "loss")
            {
                stats.Add("losses", thePlayer.Losses + 1 + "");
                stats.Add("streak", "0");
                stats.Add("didWin", "N");
                stats.Add("ratio", calcRatio(thePlayer.Wins, thePlayer.Losses + 1));
            }
            else if (status == "draw")
            {
                Console.Write("In pdba UpdatePlayer - status draw");
                stats.Add("draws", thePlayer.Draws + 1 + "");
                stats.Add("streak", "0");
                stats.Add("didWin", "N");
            }
            return db.Update("player", stats, "name='" + thePlayer.Name + "'");
        }

    }
}
