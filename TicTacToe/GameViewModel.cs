﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Data;
using System.Threading;
using System.Collections.ObjectModel;
using TicTacToe.DomainLayer;
using TicTacToe.DBLayer;
using System.ComponentModel;
using PostSharp.Patterns.Model;
using System.Windows.Shapes;
using System.Windows.Input;
using System.Windows.Controls;

namespace TicTacToe.ViewModels
{
    [NotifyPropertyChanged]

    class GameViewModel
    {
        private string _winnerTxt, _plName, _p2Name;
        private bool _winner;
        private string _rectTxt1, _rectTxt2, _rectTxt3, _rectTxt4, _rectTxt5, _rectTxt6, _rectTxt7, _rectTxt8, _rectTxt9;
        private Visibility _isLine1Shown = Visibility.Hidden;
        private Visibility _isLine2Shown = Visibility.Hidden;
        private Visibility _isLine3Shown = Visibility.Hidden;
        private Visibility _isLine4Shown = Visibility.Hidden;
        private Visibility _isLine5Shown = Visibility.Hidden;
        private Visibility _isLine6Shown = Visibility.Hidden;
        private Visibility _isLine7Shown = Visibility.Hidden;
        private Visibility _isLine8Shown = Visibility.Hidden;
        private Visibility _isHSButtonShown = Visibility.Hidden;
        private Visibility _isSaveButtonShown = Visibility.Visible;
        private Game _theGame;
        private PlayerDBA pdba;
        private GameDBA gdba;
        private DataTable _dtGameList;

        public GameViewModel(String p1, String p2, Game loadedGame)
        {
            pdba = new PlayerDBA();
            gdba = new GameDBA();
            _winner = false;
            RectangleClickCmd = new RelayCommand(new Action<object>(ShowXandO));
            if (loadedGame == null)
            {
                TheGame = new Game();
            }
            else
            {
                TheGame = loadedGame;

                if (TheGame.States[0] != "N")
                    RectTxt1 = TheGame.States[0];
                else
                    RectTxt1 = "";
                if (TheGame.States[1] != "N")
                    RectTxt2 = TheGame.States[1];
                else
                    RectTxt2 = "";
                if (TheGame.States[2] != "N")
                    RectTxt3 = TheGame.States[2];
                else
                    RectTxt3 = "";
                if (TheGame.States[3] != "N")
                    RectTxt4 = TheGame.States[3];
                else
                    RectTxt4 = "";
                if (TheGame.States[4] != "N")
                    RectTxt5 = TheGame.States[4];
                else
                    RectTxt5 = "";
                if (TheGame.States[5] != "N")
                    RectTxt6 = TheGame.States[5];
                else
                    RectTxt6 = "";
                if (TheGame.States[6] != "N")
                    RectTxt7 = TheGame.States[6];
                else
                    RectTxt7 = "";
                if (TheGame.States[7] != "N")
                    RectTxt8 = TheGame.States[7];
                else
                    RectTxt8 = "";
                if (TheGame.States[8] != "N")
                    RectTxt9 = TheGame.States[8];
                else
                    RectTxt9 = "";
            }
            P1Name = p1;
            P2Name = p2;
            TheGame.Player1 = pdba.GetPlayer(P1Name);
            TheGame.Player2 = pdba.GetPlayer(P2Name);
            SaveClickCmd = new RelayCommand(new Action<object>(SaveGame));
            DTGameList = gdba.GetGamesDataTable();
        }

        public Visibility IsLine1Shown
        {
            get { return _isLine1Shown; }
            set
            {
                _isLine1Shown = value;
            }
        }

        public Visibility IsLine2Shown
        {
            get { return _isLine2Shown; }
            set
            {
                _isLine2Shown = value;
            }
        }
        public Visibility IsLine3Shown
        {
            get { return _isLine3Shown; }
            set
            {
                _isLine3Shown = value;
            }
        }

        public Visibility IsLine4Shown
        {
            get { return _isLine4Shown; }
            set
            {
                _isLine4Shown = value;
            }
        }
        public Visibility IsLine5Shown
        {
            get { return _isLine5Shown; }
            set
            {
                _isLine5Shown = value;
            }
        }
        public Visibility IsLine6Shown
        {
            get { return _isLine6Shown; }
            set
            {
                _isLine6Shown = value;
            }
        }
        public Visibility IsLine7Shown
        {
            get { return _isLine7Shown; }
            set
            {
                _isLine7Shown = value;
            }
        }
        public Visibility IsLine8Shown
        {
            get { return _isLine8Shown; }
            set
            {
                _isLine8Shown = value;
            }
        }

        public Visibility IsHSButtonShown
        {
            get { return _isHSButtonShown; }
            set
            {
                _isHSButtonShown = value;
                if (value == Visibility.Visible)
                    IsSaveButtonShown = Visibility.Hidden;
                else
                    IsSaveButtonShown = Visibility.Visible;

            }
        }

        public Visibility IsSaveButtonShown
        {
            get { return _isSaveButtonShown; }
            set
            {
                _isSaveButtonShown = value;
            }
        }

        public void ShowXandO(object obj)
        {

            int theTurn = TheGame.getTurn();
            if (theTurn < 9)
            {
                switch (obj.ToString())
                {
                    case "1":
                        if (RectTxt1 != "O" && RectTxt1 != "X")
                        {
                            TheGame.incTurn();
                            if (!Winner)
                            {
                                if (theTurn % 2 == 1)
                                {
                                    TheGame.States[0] = "O";
                                    RectTxt1 = "O";
                                }
                                else
                                {
                                    TheGame.States[0] = "X";
                                    RectTxt1 = "X";
                                }
                            }
                        }
                        break;
                    case "2":
                        if (RectTxt2 != "O" && RectTxt2 != "X")
                        {

                            if (!Winner)
                            {
                                TheGame.incTurn();
                                if (theTurn % 2 == 1)
                                {
                                    TheGame.States[1] = "O";
                                    RectTxt2 = "O";
                                }
                                else
                                {
                                    TheGame.States[1] = "X";
                                    RectTxt2 = "X";
                                }
                            }
                        }
                        break;
                    case "3":
                        if (RectTxt3 != "O" && RectTxt3 != "X")
                        {

                            if (!Winner)
                            {
                                TheGame.incTurn();
                                if (theTurn % 2 == 1)
                                {
                                    TheGame.States[2] = "O";
                                    RectTxt3 = "O";
                                }
                                else
                                {
                                    TheGame.States[2] = "X";
                                    RectTxt3 = "X";
                                }
                            }
                        }
                        break;
                    case "4":
                        if (RectTxt4 != "O" && RectTxt4 != "X")
                        {

                            if (!Winner)
                            {
                                TheGame.incTurn();
                                if (theTurn % 2 == 1)
                                {
                                    TheGame.States[3] = "O";
                                    RectTxt4 = "O";
                                }
                                else
                                {
                                    TheGame.States[3] = "X";
                                    RectTxt4 = "X";
                                }
                            }
                        }
                        break;
                    case "5":
                        if (RectTxt5 != "O" && RectTxt5 != "X")
                        {

                            if (!Winner)
                            {
                                TheGame.incTurn();
                                if (theTurn % 2 == 1)
                                {
                                    TheGame.States[4] = "O";
                                    RectTxt5 = "O";
                                }
                                else
                                {
                                    TheGame.States[4] = "X";
                                    RectTxt5 = "X";
                                }
                            }
                        }
                        break;
                    case "6":
                        if (RectTxt6 != "O" && RectTxt6 != "X")
                        {

                            if (!Winner)
                            {
                                TheGame.incTurn();
                                if (theTurn % 2 == 1)
                                {
                                    TheGame.States[5] = "O";
                                    RectTxt6 = "O";
                                }
                                else
                                {
                                    TheGame.States[5] = "X";
                                    RectTxt6 = "X";
                                }
                            }
                        }
                        break;
                    case "7":
                        if (RectTxt7 != "O" && RectTxt7 != "X")
                        {
                            if (!Winner)
                            {
                                TheGame.incTurn();
                                if (theTurn % 2 == 1)
                                {
                                    TheGame.States[6] = "O";
                                    RectTxt7 = "O";
                                }
                                else
                                {
                                    TheGame.States[6] = "X";
                                    RectTxt7 = "X";
                                }
                            }
                        }
                        break;
                    case "8":
                        if (RectTxt8 != "O" && RectTxt8 != "X")
                        {

                            if (!Winner)
                            {
                                TheGame.incTurn();
                                if (theTurn % 2 == 1)
                                {
                                    TheGame.States[7] = "O";
                                    RectTxt8 = "O";
                                }
                                else
                                {
                                    TheGame.States[7] = "X";
                                    RectTxt8 = "X";
                                }
                            }
                        }
                        break;
                    case "9":
                        if (RectTxt9 != "O" && RectTxt9 != "X")
                        {

                            if (!Winner)
                            {
                                TheGame.incTurn();
                                if (theTurn % 2 == 1)
                                {
                                    TheGame.States[8] = "O";
                                    RectTxt9 = "O";
                                }
                                else
                                {
                                    TheGame.States[8] = "X";
                                    RectTxt9 = "X";
                                }
                            }
                        }
                        break;
                }
                CheckForWin();
            }
            if (theTurn == 8 && !Winner)
            {
                WinnerTxt = "It's a Draw!";
                // pdba.UpdatePlayer(TheGame.Player1, "draw");
                //pdba.UpdatePlayer(TheGame.Player2, "draw");
                //GetStats();
                IsHSButtonShown = Visibility.Visible;

            }
        }

        private void CheckForWin()
        {
            //horizontal wins
            if ((RectTxt1 == "O" && RectTxt2 == "O" && RectTxt3 == "O") || (RectTxt1 == "X" && RectTxt2 == "X" && RectTxt3 == "X"))
            {
                if (RectTxt1 == "X")
                {
                    WinnerTxt = "Winner: " + TheGame.Player1.Name;
                    Winner = true;
                }
                else
                {
                    WinnerTxt = "Winner: " + TheGame.Player2.Name;
                    Winner = true;
                }
                IsLine1Shown = Visibility.Visible;
            }
            else if ((RectTxt4 == "O" && RectTxt5 == "O" && RectTxt6 == "O") || (RectTxt4 == "X" && RectTxt5 == "X" && RectTxt6 == "X"))
            {
                if (RectTxt4 == "X")
                {
                    WinnerTxt = "Winner: " + TheGame.Player1.Name;
                    Winner = true;
                }
                else
                {
                    WinnerTxt = "Winner: " + TheGame.Player2.Name;
                    Winner = true;
                }
                IsLine2Shown = Visibility.Visible;
            }
            else if ((RectTxt7 == "O" && RectTxt8 == "O" && RectTxt9 == "O") || (RectTxt7 == "X" && RectTxt8 == "X" && RectTxt9 == "X"))
            {
                if (RectTxt7 == "X")
                {
                    WinnerTxt = "Winner: " + TheGame.Player1.Name;
                    Winner = true;
                }
                else
                {
                    WinnerTxt = "Winner: " + TheGame.Player2.Name;
                    Winner = true;
                }
                IsLine3Shown = Visibility.Visible;
            }

            //vertical wins
            else if ((RectTxt1 == "O" && RectTxt4 == "O" && RectTxt7 == "O") || (RectTxt1 == "X" && RectTxt4 == "X" && RectTxt7 == "X"))
            {
                if (RectTxt1 == "X")
                {
                    WinnerTxt = "Winner: " + TheGame.Player1.Name;
                    Winner = true;
                }
                else
                {
                    WinnerTxt = "Winner: " + TheGame.Player2.Name;
                    Winner = true;
                }
                IsLine4Shown = Visibility.Visible;
            }
            else if ((RectTxt2 == "O" && RectTxt5 == "O" && RectTxt8 == "O") || (RectTxt2 == "X" && RectTxt5 == "X" && RectTxt8 == "X"))
            {
                if (RectTxt2 == "X")
                {
                    WinnerTxt = "Winner: " + TheGame.Player1.Name;
                    Winner = true;
                }
                else
                {
                    WinnerTxt = "Winner: " + TheGame.Player2.Name;
                    Winner = true;
                }
                IsLine5Shown = Visibility.Visible;
            }
            else if ((RectTxt3 == "O" && RectTxt6 == "O" && RectTxt9 == "O") || (RectTxt3 == "X" && RectTxt6 == "X" && RectTxt9 == "X"))
            {
                if (RectTxt3 == "X")
                {
                    WinnerTxt = "Winner: " + TheGame.Player1.Name;
                    Winner = true;
                }
                else
                {
                    WinnerTxt = "Winner: " + TheGame.Player2.Name;
                    Winner = true;
                }
                IsLine6Shown = Visibility.Visible;
            }

            //diagonal wins
            else if ((RectTxt1 == "O" && RectTxt5 == "O" && RectTxt9 == "O") || (RectTxt1 == "X" && RectTxt5 == "X" && RectTxt9 == "X"))
            {
                if (RectTxt1 == "X")
                {
                    WinnerTxt = "Winner: " + TheGame.Player1.Name;
                    Winner = true;
                }
                else
                {
                    WinnerTxt = "Winner: " + TheGame.Player2.Name;
                    Winner = true;
                }
                IsLine7Shown = Visibility.Visible;
            }
            else if ((RectTxt3 == "O" && RectTxt5 == "O" && RectTxt7 == "O") || (RectTxt3 == "X" && RectTxt5 == "X" && RectTxt7 == "X"))
            {
                if (RectTxt3 == "X")
                {
                    WinnerTxt = "Winner: " + TheGame.Player1.Name;
                    Winner = true;
                }
                else
                {
                    WinnerTxt = "Winner: " + TheGame.Player2.Name;
                    Winner = true;
                }
                IsLine8Shown = Visibility.Visible;
            }
            if (Winner)
            {
                if (TheGame.getTurn() % 2 == 1)
                {
                    pdba.UpdatePlayer(TheGame.Player1, "win");
                    pdba.UpdatePlayer(TheGame.Player2, "loss");
                }
                else
                {
                     pdba.UpdatePlayer(TheGame.Player1, "loss");
                     pdba.UpdatePlayer(TheGame.Player2, "win");
                }
                
                IsHSButtonShown = Visibility.Visible;

            }
        }


        public bool Winner
        {
            get { return _winner; }
            set
            {
                if (value != _winner)
                {
                    _winner = value;
                }
            }
        }

        public String RectTxt1
        {
            get { return _rectTxt1; }
            set
            {
                if (value != _rectTxt1 && !Winner)
                    _rectTxt1 = value;
            }
        }

        public String RectTxt2
        {
            get { return _rectTxt2; }
            set
            {
                if (value != _rectTxt2 && !Winner)
                    _rectTxt2 = value;
            }
        }

        public String RectTxt3
        {
            get { return _rectTxt3; }
            set
            {
                if (value != _rectTxt3 && !Winner)
                    _rectTxt3 = value;
            }
        }

        public String RectTxt4
        {
            get { return _rectTxt4; }
            set
            {
                if (value != _rectTxt4 && !Winner)
                    _rectTxt4 = value;
            }
        }

        public String RectTxt5
        {
            get { return _rectTxt5; }
            set
            {
                if (value != _rectTxt5 && !Winner)
                    _rectTxt5 = value;
            }
        }

        public String RectTxt6
        {
            get { return _rectTxt6; }
            set
            {
                if (value != _rectTxt6 && !Winner)
                    _rectTxt6 = value;
            }
        }
        public String RectTxt7
        {
            get { return _rectTxt7; }
            set
            {
                if (value != _rectTxt7 && !Winner)
                    _rectTxt7 = value;
            }
        }

        public String RectTxt8
        {
            get { return _rectTxt8; }
            set
            {
                if (value != _rectTxt8 && !Winner)
                    _rectTxt8 = value;
            }
        }

        public String RectTxt9
        {
            get { return _rectTxt9; }
            set
            {
                if (value != _rectTxt9 && !Winner)
                    _rectTxt9 = value;
            }
        }

        public String WinnerTxt
        {
            get { return _winnerTxt; }
            set
            {
                if (value != _winnerTxt)
                {
                    _winnerTxt = value;
                }
            }
        }

        public String P1Name
        {
            get { return _plName; }
            set
            {
                if (value != _plName)
                {
                    _plName = value;
                }
            }
        }

        public String P2Name
        {
            get { return _p2Name; }
            set
            {
                if (value != _p2Name)
                {
                    _p2Name = value;
                }
            }
        }

        public Game TheGame
        {
            get { return _theGame; }
            set
            {
                if (value != _theGame)
                {
                    _theGame = value;
                }
            }
        }

        private ICommand _rectangleClickCmd;
        public ICommand RectangleClickCmd
        {
            get
            {
                return _rectangleClickCmd;
            }
            set
            {
                _rectangleClickCmd = value;
            }
        }

        public DataTable DTGameList
        {
            get { return _dtGameList; }
            set
            {
                if (value != _dtGameList)
                {
                    _dtGameList = value;
                }
            }
        }

        public void SaveGame(object obj)
        {

            if (gdba.SaveGame(TheGame))
                MessageBox.Show("Game saved!");
            else
                MessageBox.Show("Save failed! Try again...");
            _dtGameList = gdba.GetGamesDataTable();
        }

        private ICommand _saveClickCmd;
        public ICommand SaveClickCmd
        {
            get
            {
                return _saveClickCmd;
            }
            set
            {
                _saveClickCmd = value;
            }
        }
    }
}
