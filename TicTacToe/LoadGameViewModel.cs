﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Data;
using System.Threading;
using System.Collections.ObjectModel;
using TicTacToe.DomainLayer;
using TicTacToe.DBLayer;
using System.ComponentModel;
using PostSharp.Patterns.Model;
using System.Windows.Shapes;
using System.Windows.Input;
using System.Windows.Controls;

namespace TicTacToe.ViewModels
{
    class LoadGameViewModel
    {
        private DataTable _dtGameList;
        private GameDBA gdba;
        private Game _theGame;
        public LoadGameViewModel()
        {
            gdba = new GameDBA();
            DTGameList = gdba.GetGamesDataTable();
            LoadClickCmd = new RelayCommand(new Action<object>(LoadGame));
        }

        public DataTable DTGameList
        {
            get { return _dtGameList; }
            set
            {
                if (value != _dtGameList)
                {
                    _dtGameList = value;
                }
            }
        }

        public Game TheGame
        {
            get { return _theGame; }
            set
            {
                if (value != _theGame)
                {
                    _theGame = value;
                }
            }
        }

         private ICommand _loadClickCmd;
        public ICommand LoadClickCmd
        {
            get
            {
                return _loadClickCmd;
            }
            set
            {
                _loadClickCmd = value;
            }
        }

        public void LoadGame(object obj)
        {
            if (obj.ToString() != "")
            {
                String P1Name;
                String P2Name;
                TheGame = gdba.LoadGame(Int32.Parse(obj.ToString()));
                if (TheGame != null)
                {
                    MessageBox.Show("Game loaded!");
                    P1Name = TheGame.Player1.Name;
                    P2Name = TheGame.Player2.Name;

                    (Application.Current.MainWindow.FindName("contentControl") as ContentControl).Content = new GameView(P1Name,P2Name,TheGame);
                }
                else
                    MessageBox.Show("No such game number! Try again...");
            }
        }

    }
}
