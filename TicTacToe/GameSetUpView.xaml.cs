﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TicTacToe.ViewModels;

namespace TicTacToe
{
    /// <summary>
    /// Interaction logic for NewGameUserControl.xaml
    /// </summary>
    public partial class GameSetUpView : UserControl
    {
        public GameSetUpView()
        {
            InitializeComponent();
            this.DataContext = new GameSetupViewModel();

        }


        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            (Application.Current.MainWindow.FindName("contentControl") as ContentControl).Content = new GameView(player1Combo.SelectedValue.ToString(), player2Combo.SelectedValue.ToString(),null);
        }



       


        
    } 
}
