﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;
using System.Windows;
using System.IO;
using TicTacToe;


namespace TicTacToe.DBLayer
{
    class DBConn
    {

        private string dbConnection = "Data Source=tictactoe.sqlite;Version=3;";

        public DBConn(string DBDirectory, String DBFile)
        {
            string curFile = @"tictactoe.sqlite";
            if (!File.Exists(curFile))
            {
                MessageBox.Show("DB doesn't exist... Setting it up now!");
                SQLiteConnection.CreateFile("tictactoe.sqlite");
            }
            SQLiteConnection conn = new SQLiteConnection(dbConnection);


        }

        //run sql query against the database and return a datatable
        public DataTable GetDataTable(string theQuery)
        {
            DataTable dtab = new DataTable();
            try
            {
                SQLiteConnection conn = new SQLiteConnection(dbConnection);
                conn.Open();
                SQLiteCommand cmd = new SQLiteCommand(conn);
                cmd.CommandText = theQuery;
                SQLiteDataReader dreader = cmd.ExecuteReader();
                dtab.Load(dreader);
                dreader.Close();
                conn.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                MessageBox.Show(e.Message);
            }
            return dtab;
        }

        //returns single record from DB
        public string ExecuteScalar(string theQuery)
        {
            SQLiteConnection conn = new SQLiteConnection(dbConnection);
            conn.Open();
            SQLiteCommand cmd = new SQLiteCommand(conn);
            cmd.CommandText = theQuery;
            object value = cmd.ExecuteScalar();
            conn.Close();
            if (value != null)
            {
                return value.ToString();
            }
            return "";
        }

        //  Allows interaction with the database for non-queries.  
        public int ExecuteNonQuery(string theQuery)
        {
            SQLiteConnection conn = new SQLiteConnection(dbConnection);
            conn.Open();
            SQLiteCommand cmd = new SQLiteCommand(conn);
            cmd.CommandText = theQuery;
            int rowsUpdated = cmd.ExecuteNonQuery();
            conn.Close();
            return rowsUpdated;
        }

        //update records - usage: Update('seller',Dictionary<'columns to update','new values'>,'seller_id = 10');
        public bool Update(String tableName, Dictionary<String, String> data, String where)
        {
            String vals = "";
            Boolean returnCode = true;
            if (data.Count >= 1)
            {
                foreach (KeyValuePair<String, String> val in data)
                {
                    vals += String.Format(" {0} = '{1}',", val.Key.ToString(), val.Value.ToString());
                }
                vals = vals.Substring(0, vals.Length - 1);
            }
            try
            {
                this.ExecuteNonQuery(String.Format("update {0} set {1} where {2};", tableName,
                                       vals, where));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                MessageBox.Show(e.Message);
                returnCode = false;
            }
            return returnCode;
        }

        //insert a record - Usage: Insert('seller', Dictionary<'column names','values'>);
        public bool Insert(String tableName, Dictionary<String, String> data)
        {
            String columns = "";
            String values = "";
            Boolean returnCode = true;
            foreach (KeyValuePair<String, String> val in data)
            {
                columns += String.Format(" {0},", val.Key.ToString());
                values += String.Format(" '{0}',", val.Value);
            }
            columns = columns.Substring(0, columns.Length - 1);
            values = values.Substring(0, values.Length - 1);

            //we want this to silently but gracefully fail if initial dummy records already exist, but to proceed if they dont
            try
            {
                this.ExecuteNonQuery(String.Format("insert into {0}({1}) values({2});", tableName, columns, values));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                //MessageBox.Show(e.Message);
                returnCode = false;
            }
            return returnCode;
        }




        public bool TestConnection()
        {
            using (SQLiteConnection conn = new SQLiteConnection(dbConnection))
            {
                try
                {
                    conn.Open();
                    MessageBox.Show("Database opened... Yay!");
                    return true;
                }
                catch
                {
                    MessageBox.Show("Boooo! Database connection test failed.");
                    return false;
                }
                finally
                {
                    // Close the database connection  
                    if ((conn != null) && (conn.State != ConnectionState.Open))
                        conn.Close();
                }
            }
        }

    }
}
